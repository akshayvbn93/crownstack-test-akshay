import { ADD_TODO, TOGGLE_TODO, SET_VISIBILITY_FILTER } from "../constants/actionTypes"

let TodoId = 2

export const addTodo = text => ({
    type: ADD_TODO,
    id: TodoId++,
    text
})

export const toggleTodo = (id) => ({
    type: TOGGLE_TODO,
    id: id
})

// export const setVisibilityFilter = filter => ({
//   type: SET_VISIBILITY_FILTER,
//   filter
// })