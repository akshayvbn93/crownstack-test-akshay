import React from 'react';
import './App.css';
import Todos from './components/Todos';

const App = () =>
  <div className="app">
    <Todos/>
  </div>
 
export default App;