import React, {useState} from 'react';
import './Todos.css';
import TodoItem from '../TodoItem';
import { connect } from 'react-redux';
import {  addTodo, toggleTodo } from '../../actions';
import { SHOW_ALL, SHOW_COMPLETED, SHOW_ACTIVE } from '../../constants/actionTypes';

const Todos = props =>{

const [state, setState] = useState({
  newTodo: "",
});

const inputCapture = event => {
  setState({
    ...state,
    [event.target.name]: event.target.value
  });
};

const handleAddNewTodo = async (event) =>{
  event.preventDefault();
  if(state.newTodo){
    await props.addTodo(state.newTodo);
  }
  setState({newTodo:''})
}

const getCompletedTodos = todos =>{
  let temp = todos.filter(item=>item.completed===true);
  return temp.length;
}

return(
  <div className="todos">
    <div className="add_todo">
    <input type="text" name="newTodo" value={state.newTodo} className="todo_input" onChange={event=>inputCapture(event)}/>
    <input type="submit" value="Add Todo" onClick={(event)=>handleAddNewTodo(event)}/>
    </div>

    {props.todos.length>0&&
    <h5>Total Todos completed: {getCompletedTodos(props.todos)} out of {props.todos.length}</h5> 
}
    {(props.todos || []).map(todoObj =>
      <TodoItem
        key={todoObj.id}
        todo={todoObj}
        toggleActiveInactive={props.toggleTodo}
      />
    )}
  </div>
)}
  
const getVisibleTodos = (todos, filter) => {
  switch (filter) {
    case SHOW_ALL:
      return todos;
    case SHOW_COMPLETED:
      return todos.filter(t => t.completed);
    case SHOW_ACTIVE:
      return todos.filter(t => !t.completed);
    default:
      return todos;
  }
};

const mapStateToProps = state => {
  return { todos: getVisibleTodos(state.todos, state.visibilityFilter),
    visibilityFilter: state.visibilityFilter
 };
};

const mapDispatchToProps = dispatch => ({
  addTodo: newTodo => dispatch(addTodo(newTodo)),
  toggleTodo: id => dispatch(toggleTodo(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Todos);