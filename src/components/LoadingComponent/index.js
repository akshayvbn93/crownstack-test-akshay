import React from 'react';
import './loading-component.css';

const LoadingComponent = props =>{

return(
  <div className="container">
      <i class="fas fa-spinner fa-spin"></i>
  </div>
)}

export default LoadingComponent;