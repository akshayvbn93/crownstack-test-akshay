import React from 'react';
import './TodoItem.css';

const TodoItem = ({ todo,isDone,toggleActiveInactive }) => {
  
    const {
      id,
      text,
      completed
  } = todo;

  return (
    <div className="todo_item">
        <button
          type="button"
          className="button-inline"
          onClick={() => toggleActiveInactive(id)}
        >
          {completed?
              <p className="todo_title"><strike >{text}</strike></p>:
                  
          <p className="todo_title">{text}</p>
}
        </button>
    </div>
  );
}

export default TodoItem;