import { ADD_TODO, TOGGLE_TODO } from "../constants/actionTypes"
import { addTodo, toggleTodo, setVisibilityFilter } from "../actions"

describe('actions', () => {
  it('should create an action to add a todo', () => {
    const text = 'Finish docs'
    const expectedAction = {
      type: ADD_TODO,
      id: 2,
      text
    }
    expect(addTodo(text)).toEqual(expectedAction)
  })
})

describe('actions', () => {
    it('should create an action to toggle a todo status active/inactive', () => {
      const id = 2
      const expectedAction = {
        type: TOGGLE_TODO,
        id
      }
      expect(toggleTodo(id)).toEqual(expectedAction)
    })
  })